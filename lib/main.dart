import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intro_ui/Helpers/ColorsSys.dart';
import 'package:intro_ui/Helpers/Strings.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PageController _pageController;
  int _currentIndex = 0;

  @override
  void initState() {
    _pageController = PageController(
      initialPage: 0
    );
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20, top: 20),
            child: Text("Skip",
              style: TextStyle(
                color: ColorSys.primary,
                fontSize: 18,
                fontWeight: FontWeight.w600
              ),
            ),
          )
        ],
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          PageView(
            controller: _pageController,
            onPageChanged: (int page){
              setState(() {
                _currentIndex = page;
              });
            },
            children: <Widget>[
              MakePage(
                image: 'assets/images/step-one.png',
                title: Strings.stepOneTitle,
                content: Strings.stepOneContent
              ),
              MakePage(
                  reverse: true,
                  image: 'assets/images/step-two.png',
                  title: Strings.stepOneTitle,
                  content: Strings.stepOneContent
              ),
              MakePage(
                  image: 'assets/images/step-three.png',
                  title: Strings.stepOneTitle,
                  content: Strings.stepOneContent
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(bottom: 140),
            child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _buildIndicator(),),)
        ],
      ),
    );
  }



  Widget MakePage({image, title, content, reverse = false}){
    return Container(
      padding: EdgeInsets.only(left: 50, right: 50),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          !reverse ?
          Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Image.asset(image),
              ),
              SizedBox(height: 30,),
            ],
          ) : SizedBox(),
          Text(title, style: TextStyle(
            fontSize: 30,
            color: ColorSys.primary,
            fontWeight: FontWeight.bold
          ),),
          SizedBox(height: 20,),
          Text(content, textAlign: TextAlign.center, style: TextStyle(
              fontSize: 20,
              color: ColorSys.gray,
              fontWeight: FontWeight.w400
          ),),
          reverse ?
          Column(
            children: <Widget>[
              SizedBox(height: 30,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Image.asset(image),
              ),
            ],
          ) : SizedBox(),
        ],
      ),
    );
  }

  Widget _indicator(bool isActive){
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      height: 8,
      width: isActive ? 30 : 8,
      margin: EdgeInsets.only(right: 5),
      decoration: BoxDecoration(
        color: ColorSys.secoundry ,
        borderRadius: BorderRadius.circular(5)
      ),
    );
  }

  List<Widget> _buildIndicator(){
    List<Widget> _indicators = [];
    for(int i = 0; i < 3; i++){
      if (_currentIndex == i) {
        _indicators.add(_indicator(true));
      } else{
        _indicators.add(_indicator(false));
      }
    }
    return _indicators;
  }
}

